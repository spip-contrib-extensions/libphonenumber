<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
	//E
	'entrer_un_numero' => 'Entrez un numéro de téléphone pour le vérifier',
	'erreur_internationalisation' => 'Erreur dans l\'internationalisation du numéro', 
	'explication_pays'=> '<small><i>Le pays est nécessaire pour internationaliser le numéro de téléphone</i></small>',
	
	//F
	'forcer_telephone' => 'Faut-il vérifier le téléphone ?',
	'forcer_telephone_case' => 'Non, ne pas tenir compte d\'une erreur dans le numéro de téléphone',
	
	// L
	'label_pays' => 'Pays',
	'label_telephone' => 'Téléphone',
	'libphonenumber_titre' => 'libphonenumber for SPIP',
	
	//V
	'verifier_ce_telephone' => 'Vérifier ce téléphone',
	
	
	);
